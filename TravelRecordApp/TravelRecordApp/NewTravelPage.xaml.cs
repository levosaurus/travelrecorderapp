﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Geolocator;
using SQLite;
using TravelRecordApp.Logic;
using TravelRecordApp.Model;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TravelRecordApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewTravelPage : ContentPage
    {
        public NewTravelPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            var locator = CrossGeolocator.Current;
            var position = await locator.GetPositionAsync();

            var venues = await VenueLogic.GetVenues(position.Latitude, position.Longitude);
            VenuesListView.ItemsSource = venues;
        }

        private async void SaveToolbarItem_OnClicked(object sender, EventArgs e)
        {
            try
            {

                var selectedVenue = VenuesListView.SelectedItem as Venue;
                var firstCategory = selectedVenue.categories.FirstOrDefault();
                Post post = new Post
                {
                    Experience = ExperienceEntry.Text,
                    CategoryId = firstCategory.id,
                    CategoryNamme = firstCategory.name,
                    Address = selectedVenue.location.address,
                    Distance = selectedVenue.location.distance,
                    Latitude = selectedVenue.location.lat,
                    Longitude = selectedVenue.location.lng,
                    VenueName = selectedVenue.name,
                    UserId = App.User.Id

                };

                /*using (SQLiteConnection conn = new SQLiteConnection(App.DatabaseLocation))
                {
                    conn.CreateTable<Post>();
                    var rows = conn.Insert(post);

                    if (rows > 0)
                        DisplayAlert("Success", "Experience Successfuly inserted", "Ok");
                    else
                        DisplayAlert("Failure", "Experience failed to be inserted", "Ok");

                }*/
                await App.MobileService.GetTable<Post>().InsertAsync(post);
                await DisplayAlert("Success", "Experience Successfuly inserted", "Ok");


            }
            catch (NullReferenceException)
            {
                //bypass
                await DisplayAlert("Failure", "Experience failed to be inserted", "Ok");

            }
            catch (Exception exception)
            {
                Debug.WriteLine("--------------------------------- " + exception.Message);
                await DisplayAlert("Failure", "Experience failed to be inserted", "Ok");

            }

        }
    }
}