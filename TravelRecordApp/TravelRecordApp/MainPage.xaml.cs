﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRecordApp.Model;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TravelRecordApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            var assembly = typeof(MainPage);

            IconImage.Source = ImageSource.FromResource("TravelRecordApp.Assets.Images.plane.png", assembly);
        }

        private async void LoginBtn_Clicked(object sender, EventArgs e)
        {

            bool isEmailEmpty = string.IsNullOrEmpty(EmailEntry.Text);
            bool isPassEmpty = string.IsNullOrEmpty(PassEntry.Text);

            if (isPassEmpty || isEmailEmpty)
            {
               
            }
            else
            {
                var user = (await App.MobileService.GetTable<User>().Where(u => u.Email == EmailEntry.Text).ToListAsync()).FirstOrDefault();

                if (user != null)
                {
                    if(user.Password == PassEntry.Text)
                    {
                        App.User = user;
                        await Navigation.PushAsync(new HomePage());
                    }
                    else
                    {
                        await DisplayAlert("Error", "Email or password are incorrect", "Ok");
                    }
                }
                else
                {
                        await DisplayAlert("Error", "There was an Error logging you in", "Ok");

                }

            }
           // DisplayAlert(EmailEntry.Text, PassEntry.Text, "Ok");
        }

        private void RegisterBtn_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RegisterPage()); 
        }
    }
}
