﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRecordApp.Model;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TravelRecordApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
        }

        private async void RegisterBtn_OnClicked(object sender, EventArgs e)
        {
            if (string.Equals(PassEntry.Text, ConfirmPassEntry.Text, StringComparison.CurrentCulture))
            {
                User user = new User()
                {
                    Email = EmailEntry.Text,
                    Password = PassEntry.Text
                };
                await App.MobileService.GetTable<User>().InsertAsync(user);

            }
            else
            {
               await DisplayAlert("Error", "Password don't much", "Ok");
            }
        }
    }
}