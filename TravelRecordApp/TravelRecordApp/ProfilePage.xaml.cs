﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using TravelRecordApp.Model;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TravelRecordApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        public ProfilePage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            //using (SQLiteConnection conn = new SQLiteConnection(App.DatabaseLocation))
            //{

            var postsTable = await App.MobileService.GetTable<Post>().Where(u => u.UserId == App.User.Id).ToListAsync();

            var categories = (from p in postsTable
                              orderby p.CategoryId
                              select p.CategoryNamme)
                             .Distinct()
                             .ToList();

            //var categories =postsTable
            //    .OrderBy(f=>f.CategoryId)
            //    .Select(f=>f.CategoryNamme)
            //    .Distinct()
            //    .ToList();

            Dictionary<string, int> categoriesCount = new Dictionary<string, int>();

            foreach (var category in categories)
            {
                var count = (from p in postsTable
                             where p.CategoryNamme == category
                             select p).ToList().Count;

                categoriesCount.Add(category, count);
            }

            CategoriesListView.ItemsSource = categoriesCount;
            PostCountLbl.Text = postsTable.Count.ToString();
            
            // }
        }
    }
}